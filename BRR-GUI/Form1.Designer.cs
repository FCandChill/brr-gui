﻿namespace BRR_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radioButton_spc2brr = new System.Windows.Forms.RadioButton();
            this.groupBox_mode = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButton_UnqBRR = new System.Windows.Forms.RadioButton();
            this.radioButton_BRR2WAV = new System.Windows.Forms.RadioButton();
            this.groupBox_action1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.button_spc2BRR = new System.Windows.Forms.Button();
            this.button_Get_Unq = new System.Windows.Forms.Button();
            this.button_GetWAV = new System.Windows.Forms.Button();
            this.groupBox_path = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox_input = new System.Windows.Forms.GroupBox();
            this.textBox_input = new System.Windows.Forms.TextBox();
            this.button_select_input_path = new System.Windows.Forms.Button();
            this.groupBox_output = new System.Windows.Forms.GroupBox();
            this.textBox_output = new System.Windows.Forms.TextBox();
            this.button_select_output_path = new System.Windows.Forms.Button();
            this.groupBox_option = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBox_recursive = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_mode.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox_action1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.groupBox_path.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBox_input.SuspendLayout();
            this.groupBox_output.SuspendLayout();
            this.groupBox_option.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButton_spc2brr
            // 
            this.radioButton_spc2brr.AutoSize = true;
            this.radioButton_spc2brr.Location = new System.Drawing.Point(3, 3);
            this.radioButton_spc2brr.Name = "radioButton_spc2brr";
            this.radioButton_spc2brr.Size = new System.Drawing.Size(88, 17);
            this.radioButton_spc2brr.TabIndex = 0;
            this.radioButton_spc2brr.Text = "SPC To BRR";
            this.radioButton_spc2brr.UseVisualStyleBackColor = true;
            this.radioButton_spc2brr.CheckedChanged += new System.EventHandler(this.RadioButton_spc2brr_CheckedChanged);
            // 
            // groupBox_mode
            // 
            this.groupBox_mode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_mode.Controls.Add(this.flowLayoutPanel1);
            this.groupBox_mode.Location = new System.Drawing.Point(12, 12);
            this.groupBox_mode.Name = "groupBox_mode";
            this.groupBox_mode.Size = new System.Drawing.Size(562, 49);
            this.groupBox_mode.TabIndex = 1;
            this.groupBox_mode.TabStop = false;
            this.groupBox_mode.Text = "Mode";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.flowLayoutPanel1.Controls.Add(this.radioButton_spc2brr);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_UnqBRR);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_BRR2WAV);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(556, 30);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // radioButton_UnqBRR
            // 
            this.radioButton_UnqBRR.AutoSize = true;
            this.radioButton_UnqBRR.Location = new System.Drawing.Point(97, 3);
            this.radioButton_UnqBRR.Name = "radioButton_UnqBRR";
            this.radioButton_UnqBRR.Size = new System.Drawing.Size(111, 17);
            this.radioButton_UnqBRR.TabIndex = 2;
            this.radioButton_UnqBRR.Text = "Grab Unique BRR";
            this.radioButton_UnqBRR.UseVisualStyleBackColor = true;
            this.radioButton_UnqBRR.CheckedChanged += new System.EventHandler(this.RadioButton_spc2brr_CheckedChanged);
            // 
            // radioButton_BRR2WAV
            // 
            this.radioButton_BRR2WAV.AutoSize = true;
            this.radioButton_BRR2WAV.Location = new System.Drawing.Point(214, 3);
            this.radioButton_BRR2WAV.Name = "radioButton_BRR2WAV";
            this.radioButton_BRR2WAV.Size = new System.Drawing.Size(92, 17);
            this.radioButton_BRR2WAV.TabIndex = 1;
            this.radioButton_BRR2WAV.Text = "BRR To WAV";
            this.radioButton_BRR2WAV.UseVisualStyleBackColor = true;
            this.radioButton_BRR2WAV.CheckedChanged += new System.EventHandler(this.RadioButton_spc2brr_CheckedChanged);
            // 
            // groupBox_action1
            // 
            this.groupBox_action1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_action1.Controls.Add(this.flowLayoutPanel3);
            this.groupBox_action1.Location = new System.Drawing.Point(12, 260);
            this.groupBox_action1.Name = "groupBox_action1";
            this.groupBox_action1.Size = new System.Drawing.Size(562, 55);
            this.groupBox_action1.TabIndex = 0;
            this.groupBox_action1.TabStop = false;
            this.groupBox_action1.Text = "Action";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.button_spc2BRR);
            this.flowLayoutPanel3.Controls.Add(this.button_Get_Unq);
            this.flowLayoutPanel3.Controls.Add(this.button_GetWAV);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(556, 36);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // button_spc2BRR
            // 
            this.button_spc2BRR.Location = new System.Drawing.Point(3, 3);
            this.button_spc2BRR.Name = "button_spc2BRR";
            this.button_spc2BRR.Size = new System.Drawing.Size(75, 23);
            this.button_spc2BRR.TabIndex = 0;
            this.button_spc2BRR.Text = "Get BRR";
            this.button_spc2BRR.UseVisualStyleBackColor = true;
            this.button_spc2BRR.Click += new System.EventHandler(this.Button_spc2BRR_Click);
            // 
            // button_Get_Unq
            // 
            this.button_Get_Unq.Location = new System.Drawing.Point(84, 3);
            this.button_Get_Unq.Name = "button_Get_Unq";
            this.button_Get_Unq.Size = new System.Drawing.Size(75, 23);
            this.button_Get_Unq.TabIndex = 1;
            this.button_Get_Unq.Text = "Get Unique";
            this.button_Get_Unq.UseVisualStyleBackColor = true;
            this.button_Get_Unq.Click += new System.EventHandler(this.Button_Get_Unq_Click);
            // 
            // button_GetWAV
            // 
            this.button_GetWAV.Location = new System.Drawing.Point(165, 3);
            this.button_GetWAV.Name = "button_GetWAV";
            this.button_GetWAV.Size = new System.Drawing.Size(75, 23);
            this.button_GetWAV.TabIndex = 1;
            this.button_GetWAV.Text = "Get WAV";
            this.button_GetWAV.UseVisualStyleBackColor = true;
            this.button_GetWAV.Click += new System.EventHandler(this.Button_GetWAV_Click);
            // 
            // groupBox_path
            // 
            this.groupBox_path.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_path.Controls.Add(this.flowLayoutPanel2);
            this.groupBox_path.Location = new System.Drawing.Point(12, 63);
            this.groupBox_path.Name = "groupBox_path";
            this.groupBox_path.Size = new System.Drawing.Size(562, 196);
            this.groupBox_path.TabIndex = 3;
            this.groupBox_path.TabStop = false;
            this.groupBox_path.Text = "Path";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.groupBox_input);
            this.flowLayoutPanel2.Controls.Add(this.groupBox_output);
            this.flowLayoutPanel2.Controls.Add(this.groupBox_option);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(556, 177);
            this.flowLayoutPanel2.TabIndex = 9;
            // 
            // groupBox_input
            // 
            this.groupBox_input.Controls.Add(this.textBox_input);
            this.groupBox_input.Controls.Add(this.button_select_input_path);
            this.groupBox_input.Location = new System.Drawing.Point(3, 3);
            this.groupBox_input.Name = "groupBox_input";
            this.groupBox_input.Size = new System.Drawing.Size(538, 55);
            this.groupBox_input.TabIndex = 7;
            this.groupBox_input.TabStop = false;
            this.groupBox_input.Text = "Input Folder";
            // 
            // textBox_input
            // 
            this.textBox_input.Location = new System.Drawing.Point(6, 19);
            this.textBox_input.Name = "textBox_input";
            this.textBox_input.Size = new System.Drawing.Size(414, 20);
            this.textBox_input.TabIndex = 1;
            this.textBox_input.TextChanged += new System.EventHandler(this.textBox_input_TextChanged);
            // 
            // button_select_input_path
            // 
            this.button_select_input_path.Location = new System.Drawing.Point(426, 17);
            this.button_select_input_path.Name = "button_select_input_path";
            this.button_select_input_path.Size = new System.Drawing.Size(75, 23);
            this.button_select_input_path.TabIndex = 0;
            this.button_select_input_path.Text = "Select Path";
            this.button_select_input_path.UseVisualStyleBackColor = true;
            this.button_select_input_path.Click += new System.EventHandler(this.Button_select_input_path_Click);
            // 
            // groupBox_output
            // 
            this.groupBox_output.Controls.Add(this.textBox_output);
            this.groupBox_output.Controls.Add(this.button_select_output_path);
            this.groupBox_output.Location = new System.Drawing.Point(3, 64);
            this.groupBox_output.Name = "groupBox_output";
            this.groupBox_output.Size = new System.Drawing.Size(538, 50);
            this.groupBox_output.TabIndex = 8;
            this.groupBox_output.TabStop = false;
            this.groupBox_output.Text = "Output Folder";
            this.groupBox_output.Visible = false;
            // 
            // textBox_output
            // 
            this.textBox_output.Location = new System.Drawing.Point(6, 19);
            this.textBox_output.Name = "textBox_output";
            this.textBox_output.Size = new System.Drawing.Size(414, 20);
            this.textBox_output.TabIndex = 3;
            // 
            // button_select_output_path
            // 
            this.button_select_output_path.Location = new System.Drawing.Point(426, 17);
            this.button_select_output_path.Name = "button_select_output_path";
            this.button_select_output_path.Size = new System.Drawing.Size(75, 23);
            this.button_select_output_path.TabIndex = 5;
            this.button_select_output_path.Text = "Select Path";
            this.button_select_output_path.UseVisualStyleBackColor = true;
            this.button_select_output_path.Click += new System.EventHandler(this.Button_select_output_path_Click);
            // 
            // groupBox_option
            // 
            this.groupBox_option.Controls.Add(this.flowLayoutPanel4);
            this.groupBox_option.Location = new System.Drawing.Point(3, 120);
            this.groupBox_option.Name = "groupBox_option";
            this.groupBox_option.Size = new System.Drawing.Size(538, 48);
            this.groupBox_option.TabIndex = 4;
            this.groupBox_option.TabStop = false;
            this.groupBox_option.Text = "Option";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.checkBox_recursive);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(532, 29);
            this.flowLayoutPanel4.TabIndex = 7;
            // 
            // checkBox_recursive
            // 
            this.checkBox_recursive.AutoSize = true;
            this.checkBox_recursive.Location = new System.Drawing.Point(3, 3);
            this.checkBox_recursive.Name = "checkBox_recursive";
            this.checkBox_recursive.Size = new System.Drawing.Size(74, 17);
            this.checkBox_recursive.TabIndex = 6;
            this.checkBox_recursive.Text = "Recursive";
            this.checkBox_recursive.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(586, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 327);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox_action1);
            this.Controls.Add(this.groupBox_path);
            this.Controls.Add(this.groupBox_mode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "BRR GUI";
            this.groupBox_mode.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox_action1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.groupBox_path.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.groupBox_input.ResumeLayout(false);
            this.groupBox_input.PerformLayout();
            this.groupBox_output.ResumeLayout(false);
            this.groupBox_output.PerformLayout();
            this.groupBox_option.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton_spc2brr;
        private System.Windows.Forms.GroupBox groupBox_mode;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton radioButton_UnqBRR;
        private System.Windows.Forms.RadioButton radioButton_BRR2WAV;
        private System.Windows.Forms.GroupBox groupBox_action1;
        private System.Windows.Forms.GroupBox groupBox_path;
        private System.Windows.Forms.CheckBox checkBox_recursive;
        private System.Windows.Forms.Button button_select_output_path;
        private System.Windows.Forms.TextBox textBox_output;
        private System.Windows.Forms.TextBox textBox_input;
        private System.Windows.Forms.Button button_select_input_path;
        private System.Windows.Forms.Button button_spc2BRR;
        private System.Windows.Forms.Button button_GetWAV;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button button_Get_Unq;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox_input;
        private System.Windows.Forms.GroupBox groupBox_output;
        private System.Windows.Forms.GroupBox groupBox_option;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
    }
}

