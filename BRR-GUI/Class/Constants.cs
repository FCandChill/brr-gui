﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public static class Constants
{
    public const int STUB = -1;

    public const string LOOP = "-loop";

    public static byte[] LoopData = new byte[] 
    {
        0x73, //s
        0x6D, //m
        0x70, //l
        0x6C, //p
        0x3C,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x12,
        0x7A,
        0x00,
        0x00,
        0x3C,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x01,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0xFF, //loop start point uint - little endian
        0xFF, //loop start point uint - little endian
        0xFF, //loop start point uint - little endian
        0xFF, //loop start point uint - little endian
        0xFF, //loop end uint - little endian
        0xFF, //loop end uint - little endian
        0xFF, //loop end uint - little endian
        0x7F, //loop end uint - little endian
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x78,
        0x74,
        0x72,
        0x61,
        0x10,
        0x00,
        0x00,
        0x00,
        0x20,
        0x00,
        0x00,
        0x00,
        0x80,
        0x00,
        0x00,
        0x01,
        0x40,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00
    };
}
