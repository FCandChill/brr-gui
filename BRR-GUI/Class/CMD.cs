﻿using System.Diagnostics;
using System.IO;

public class CMD
{
    private Process p;
    private ProcessStartInfo info;
    private StreamWriter sw;
    public CMD()
    {
        p = new Process();
        info = new ProcessStartInfo
        {
            WindowStyle = ProcessWindowStyle.Hidden,
            FileName = "cmd.exe",
            RedirectStandardInput = true,
            RedirectStandardOutput = false,
            UseShellExecute = false
        };

        p.StartInfo = info;
        p.Start();

        sw = p.StandardInput;
    }

    public void Write(string Command)
    {
        if (sw.BaseStream.CanWrite)
            sw.WriteLine(Command);
    }

    public void End()
    {
        
        //Write("exit");
        p.Dispose();
    }
}