﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace BRR_GUI
{
    public partial class Form1 : Form
    {
        /*
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;*/

        public Form1()
        {
            InitializeComponent();
            radioButton_spc2brr.Checked = true;

            //var handle = GetConsoleWindow();

            // Hide
            //ShowWindow(handle, SW_HIDE);

            // Show
            //ShowWindow(handle, SW_SHOW);


        }

        private void RadioButton_spc2brr_CheckedChanged(object sender = null, EventArgs e = null)
        {
            if (textBox_input.Text != "" /*&& textBox_output.Text != ""*/)
            {
                button_spc2BRR.Visible = radioButton_spc2brr.Checked;
                button_Get_Unq.Visible = radioButton_UnqBRR.Checked;
                button_GetWAV.Visible = radioButton_BRR2WAV.Checked;
            }
            else button_spc2BRR.Visible = button_Get_Unq.Visible = button_GetWAV.Visible = false;
        }

        private void Button_select_input_path_Click(object sender, EventArgs e)
        {
            string result = GetDirectoryPath();

            if (result != "")
            {
                textBox_input.Text = result;
            }
        }
        private void Button_select_output_path_Click(object sender, EventArgs e)
        {
            textBox_output.Text = GetDirectoryPath();
            RadioButton_spc2brr_CheckedChanged();
        }

        private string GetDirectoryPath()
        {
            string RetMe = "";
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
                fbd.SelectedPath = Environment.CurrentDirectory;
                if (fbd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    RetMe = fbd.SelectedPath;
            }
            
            return RetMe;
        }

        private void Button_spc2BRR_Click(object sender, EventArgs e)
        {
            CMD cmd = new CMD();
            try
            {
                if (DoDirectoriesExist())
                {
                    void Function(string Folder)
                    {
                        foreach (string FileName in Directory.GetFiles(Folder, "*.spc"))
                        {
                            cmd.Write(String.Format("split700.exe -L \"{0}\"", FileName));
                            Console.WriteLine(String.Format("Wrote {0}.brr.", Path.GetFileNameWithoutExtension(FileName)));
                        }

                    }

                    Function(textBox_input.Text);
                    if (checkBox_recursive.Checked)
                        foreach (string Folder in Directory.GetDirectories(textBox_input.Text))
                            Function(Folder);

                    cmd.End();
                }
                else MessageBox.Show("Directory doesn't exist.");
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString());
            }
            finally
            {
                cmd.End();
            }
        }

        private void Button_GetWAV_Click(object sender, EventArgs e)
        {
            CMD cmd = new CMD();
            try
            {
                if (DoDirectoriesExist())
                {
                    void Function(string Folder)
                    {
                        foreach (string FileName in Directory.GetFiles(Folder, "*.brr"))
                        {
                            string path = Path.GetDirectoryName(FileName) + "//";

                            string wave = String.Format("{0}.wav", path + Path.GetFileNameWithoutExtension(FileName));

                            File.Delete(wave);
                            string s = String.Format("snesbrr.exe -d \"{0}\" \"{1}\"", path + Path.GetFileName(FileName), wave);
                            cmd.Write(s);
                            string FileNameWoExt = Path.GetFileNameWithoutExtension(FileName);

                            if (GetLength(FileNameWoExt, out uint LoopPoint, out bool HasLoopPoint))
                            {
                                while (!File.Exists(wave))
                                {
                                    //MessageBox.Show(wave + " file doesn't exist.");
                                }
                                while (FileLock.IsFileLocked(new FileInfo(wave)))
                                {
                                    //MessageBox.Show(wave + " file is in use.");
                                }

                                byte[]
                                    WavData = File.ReadAllBytes(wave),
                                    LoopData = Constants.LoopData;

                                byte[] b = BitConverter.GetBytes(LoopPoint);

                                Array.Copy(b, 0, LoopData, 0x34, b.Length);
                                WavData = WavData.Concat(LoopData).ToArray();
                                File.WriteAllBytes(wave, WavData);

                            }

                            Console.WriteLine(String.Format("Wrote {0}.", Path.GetFileName(wave)));
                        }
                    }

                    Function(textBox_input.Text);
                    if (checkBox_recursive.Checked)
                        foreach (string Folder in Directory.GetDirectories(textBox_input.Text))
                            Function(Folder);
                }
                else MessageBox.Show("Directory doesn't exist.");
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString());
            }
            finally
            {
                cmd.End();
            }
        }

        private bool GetLength(string FileNameWoExt, out uint length, out bool HasLoopPoint)
        {
            bool pass = false;
            HasLoopPoint = true;
            length = 0;
            if (!FileNameWoExt.EndsWith("-noloop"))
            {
                int index = FileNameWoExt.LastIndexOf(Constants.LOOP);

                if (index != Constants.STUB)
                {
                    index += Constants.LOOP.Length;
                    string Str_LoopPoint = FileNameWoExt.Substring(index, FileNameWoExt.Length - index);
                    pass = UInt32.TryParse(Str_LoopPoint, out uint result1);
                    length = result1;
                }
            }
            else HasLoopPoint = false;

            return pass;
        }

        private int GetID(string FileName)
        {
            int start = FileName.LastIndexOf("_") + 1,
                end = FileName.LastIndexOf("-");
            string hexValue = FileName.Substring(start, end - start);
            return int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
        }

        private bool DoDirectoriesExist() => Directory.Exists(textBox_input.Text) /*&& Directory.Exists(textBox_output.Text)*/;

        private void Button_Get_Unq_Click(object sender, EventArgs e)
        {
            try
            {
                MD5 md5 = MD5.Create();

                /* Key = hash
                 * 1 string = directory
                 * 2 string = ID */
                SortedList<string, KeyValuePair<string, int>> HashList = new SortedList<string, KeyValuePair<string, int>>();

                /* Key = ID
                 * 1 string = directory */
                SortedList<int, string> HexList = new SortedList<int, string>();

                bool FollowsStrictID = true;
                if (DoDirectoriesExist())
                {
                    void Function(string Folder)
                    {
                        foreach (string FileName in Directory.GetFiles(Folder, "*.brr"))
                        {
                            using (FileStream stream = File.OpenRead(FileName))
                            {
                                GetLength(Path.GetFileNameWithoutExtension(FileName), out uint LoopPoint, out bool HasLoopPoint);

                                string FullFilePath = Path.GetFullPath(FileName);
                                string Hash = Encoding.Default.GetString(md5.ComputeHash(stream));
                                int ID = GetID(Path.GetFileNameWithoutExtension(FileName));

                                if (!HashList.ContainsKey(Hash))
                                {
                                    HashList.Add(Hash, new KeyValuePair<string, int>(FullFilePath, ID));
                                }
                                if (FollowsStrictID)
                                {
                                    if (!HexList.ContainsKey(ID))
                                    {
                                        HexList.Add(ID, FullFilePath);
                                    }
                                    //Contains the ID. Check to see if the hash matches
                                    else if (HashList.ContainsKey(Hash) && (HashList[Hash].Value != ID))
                                    {
                                        FollowsStrictID = false; //ah ha. It's a different sample
                                    }
                                }
                            }
                        }
                    }

                    string GenerateFilePathAndName(string FoldTitle, int key, string OriginalDirPath)
                    {
                        string NameWoExt = Path.GetFileNameWithoutExtension(OriginalDirPath);
                        GetLength(NameWoExt, out uint LoopPoint, out bool HasLoopPoint);

                        string LastDir = new DirectoryInfo(Path.GetDirectoryName(OriginalDirPath)).Name;

                        string FileName;
                        if (!HasLoopPoint)
                            FileName = String.Format("{0}\\{2}_{1}-noloop.brr", FoldTitle, LastDir, key.ToString("X2"));
                        else
                            FileName = String.Format("{0}\\{2}_{1}-noloop.brr", FoldTitle, LastDir, key.ToString("X2"), LoopPoint);

                        return FileName;
                    }


                    void PrepareToWriteAllSamplesToFolder(string FolderTitle, string FullPathName)
                    {
                        string NewDirectory = String.Format("{0}_BRR_unique", FullPathName);

                        Directory.CreateDirectory(NewDirectory);

                        if (FollowsStrictID)
                        {
                            foreach (KeyValuePair<int, string> kp in HexList)
                            {
                                string NewFilePath = GenerateFilePathAndName(NewDirectory, kp.Key, kp.Value);
                                File.Delete(NewFilePath);
                                File.Copy(kp.Value, NewFilePath);
                            }
                        }
                        else
                        {
                            int i = 0;
                            foreach (KeyValuePair<string, KeyValuePair<string, int>> kp in HashList)
                            {
                                string NewFilePath = GenerateFilePathAndName(NewDirectory, i, kp.Value.Key);
                                File.Delete(NewFilePath);
                                File.Copy(kp.Value.Key, NewFilePath);
                                i++;
                            }
                        }
                    }

                    string FilePathWoFile = Path.GetDirectoryName(textBox_input.Text);
                    string FullPath = Path.GetFullPath(textBox_input.Text);

                    if (!checkBox_recursive.Checked)
                    {
                        Function(textBox_input.Text);
                        PrepareToWriteAllSamplesToFolder(FilePathWoFile, FullPath);
                    }
                    else
                    {
                        Function(textBox_input.Text);
                        foreach (string Folder in Directory.GetDirectories(textBox_input.Text))
                            Function(Folder);
                        PrepareToWriteAllSamplesToFolder(FilePathWoFile, FullPath);
                    }
                }
                else MessageBox.Show("Directory or directories don't exist.");
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString());
            }
        }

        private void textBox_input_TextChanged(object sender, EventArgs e)
        { RadioButton_spc2brr_CheckedChanged(); }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "By: FCAndChill.\n\nThis program can rip BRR samples from SPC files, " +
                "remove duplicate BRR samples, and convert BRR samples to looped WAV files in bulk. " +
                "I'm not responsible for any glitches and/or damages done to your computer. " +
                "Use this software with caution and keep backups.");
        }
    }
}
