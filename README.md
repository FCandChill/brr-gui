# BRR-gui
A utility that rips BRR samples from SPCs and convert them to WAV files. This utility incorporates pre-existing utilities into it (snesbrr.exe and split700.exe).

![](https://bitbucket.org/FCandChill/brr-gui/raw/91a406fe4fe96817b419d3962eefeda007088a22/BRR-gui%20v1.png)


# What it does
* This utility can operate recursively so you can rip samples and convert them to WAV files. Therefore, you could rip BRR samples for every SNES game with a couple of mouse clicks.
* Includes the loop point in WAV files.
* Out of multiple BRR files, this utility will pick out the unique ones based on their hash. Useful when ripping an entire soundtrack.
